package com.redhat.training.jb421;

import org.apache.camel.builder.RouteBuilder;

public class FileRouteBuilder extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub		
		from("ftp::etc/file/incoming?include=order.*xml")
		.to("file:etc/file/out?fileName=journal.txt&fileExist=Append");
	}
}
